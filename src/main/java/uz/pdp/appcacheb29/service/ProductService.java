package uz.pdp.appcacheb29.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.appcacheb29.collection.Product;
import uz.pdp.appcacheb29.repository.ProductRepository;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private static final String list = "list";

    private final Map<String, Object> productsCache = new ConcurrentHashMap<>();

    public Product create(Product product) {
        productRepository.save(product);
        return product;
    }


    public Product one(String id) {
        Object object = productsCache.get(id);
        if (object != null) {
            return (Product) object;
        }

        Product product = productRepository.findById(id).orElseThrow();
        productsCache.put(id, product);
        return product;
    }

    public List<Product> list() {
        if (productsCache.get(list) != null)
            return (List<Product>) productsCache.get(list);

        List<Product> products = productRepository.findAll();
        productsCache.put(list, products);
        return products;
    }

    public Product edit(String id, Product product) {
        product.setId(id);
        productRepository.save(product);
        productsCache.put(id, product);
        productsCache.remove(list);
        return product;
    }

    public void delete(String id) {
        productRepository.deleteById(id);
        productsCache.remove(id);
        productsCache.remove(list);
    }
}
