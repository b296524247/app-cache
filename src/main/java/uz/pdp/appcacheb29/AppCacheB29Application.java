package uz.pdp.appcacheb29;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppCacheB29Application {

    public static void main(String[] args) {
        SpringApplication.run(AppCacheB29Application.class, args);
    }

}
