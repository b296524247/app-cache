package uz.pdp.appcacheb29.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import uz.pdp.appcacheb29.collection.Product;

public interface ProductRepository extends MongoRepository<Product, String> {
}
